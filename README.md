![coollogo_com-12431451.png](https://bitbucket.org/repo/5KErpq/images/3127956616-coollogo_com-12431451.png)

# README #

The documentation below will help with the successful installation of the Mechanical Deals 10 CRM/ERP System.

### What is this repository for? ###

* This repository is specifically for any/all scripts which will provide easy installation of the Mechanical Deals 10 CRM/ERP System.
* Version 10.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Follow the instructions below via the command line on your Ubuntu 16.04 server.
 
* This script will also give you the ability to define an xmlrpc_port in the .conf file that is generated under /etc/

* This script can be safely used in a multi-instance code base server because the default Mechanical Deals port is changed BEFORE the Mechanical Deals server is started.

### Installation Procedure ###


1) Download the script:
```
sudo wget https://bitbucket.org/cmc003/md_10_install_script/src/master/odoo_install.sh
```
2) Make the script executable:
```
sudo chmod +x odoo_install.sh
```
3) Execute the script:
```
sudo ./odoo_install.sh
```

### Additional Information ###

* Summary of set up *(Coming Soon)*
* Configuration  *(Coming Soon)*
* Dependencies  *(Coming Soon)*
* Database configuration  *(Coming Soon)*
* How to run tests  *(Coming Soon)*
* Deployment instructions  *(Coming Soon)*

### Who do I talk to? ###

* Christopher M. Cassidy
* Mechanical Deals